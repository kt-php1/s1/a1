<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>PHP-KT-Activity 1</title>
</head>
<body style="padding: 10%;">
	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Full Address</h1>
		<p><?php echo getFullAddress("3F Caswynn Bldg.", "Timog Avenue", "Quezon City", "Metro Manila", "Philippines"); ?></p>
		<p><?php echo getFullAddress("3F Enzo Bldg.", "Buendia Avenue", "Makati City", "Metro Manila", "Philippines"); ?></p>
	</div>

	<div style="border: 1px solid black; padding: 5%; margin: 10%;">
		<h1>Letter-Based Grading</h1>
		<p><?php echo getLetterGrade(87); ?></p>
		<p><?php echo getLetterGrade(94); ?></p>
		<p><?php echo getLetterGrade(74); ?></p>
	</div>
	
</body>
</html>